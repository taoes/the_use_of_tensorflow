#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: demo_1.py
@time: 2018/2/24 17:57
"""

# 线性回归

import tensorflow as tf
import matplotlib.pyplot as plt

# 构建数据
x_data = tf.linspace(start=-50.0,stop=50.0,num=100)
y_data = 3 * x_data + 5.3


# 构建测试集合
x_test= x_data[0:10]
y_test= y_data[0:10]


#  构建训练集
x_train = x_data[10:]
y_train = y_data[10:]


W = tf.Variable(tf.random_uniform([1],2,4))
B = tf.Variable(tf.zeros([1]))


print  W,B


# 构建线性模型
Y = W * x_data + B


cost = tf.reduce_sum(tf.pow((Y-y_data),2))
train_step = tf.train.GradientDescentOptimizer(0.1).minimize(cost)

# 初始化数据
init  =tf.global_variables_initializer()

with tf.Session() as session:
    session.run(init)
    cost_history = []
    for i in range(100):
        session.run(train_step)
        cost_history.append(session.run(cost))
        # 输出每次训练后的W,b和cost值
        print("After %d iteration:" %i)
        print("W: %f" % session.run(W))
        print("b: %f" % session.run(B))
        print("cost: %f" % session.run(cost))
    # 输出最终的W,b和cost值
    print("W_Value: %f" % session.run(W),
          "b_Value: %f" % session.run(B),
          "cost_Value: %f" % session.run(cost))
