#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: index.py
@time: 2018/2/24 14:47
"""

# 学习内容:构建Session的第二种方法

import tensorflow as tf

if __name__ == '__main__':
    num1 = tf.add(3,3)
    with tf.Session() as session:
        print session.run(num1)
