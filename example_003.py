#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: index.py
@time: 2018/2/24 14:47
"""

# 学习内容:常见的tensorFlow的数据执行方法

import tensorflow as tf

if __name__ == '__main__':
    x = 2
    y = 3
    #5
    add_op = tf.add(x, y)

    #-1
    sub_op = tf.subtract(x,y)
    #6
    mul_op = tf.multiply(x, y)
    #10
    useless = tf.multiply(x, add_op)
    #5^10
    pow_op = tf.pow(add_op, mul_op)
    with tf.Session() as sess:
        add,sub, not_useless,powop = sess.run([add_op, sub_op,useless,pow_op])
        print add
        print sub
        print  not_useless
        print powop