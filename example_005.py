#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: index.py
@time: 2018/2/24 14:47
"""

# 学习内容:向量和矩阵的生成方法

import tensorflow as tf

if __name__ == '__main__':
    x= tf.fill([2,1],2.0)
    y= tf.linspace(start=10.0,stop=13.0,num=4)
    sub = [[1.1,2.2,3.3,4.4],
           [5.5,6.6,7.7,8.8]]
    mul = tf.multiply(x,y,name="mul")
    result= tf.add(mul,sub)
    with tf.Session() as session:
        print session.run(mul)
        print session.run(result)