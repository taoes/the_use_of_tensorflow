#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: video2_001.py
@time: 2018/2/25 09:54
TensorFlow中变量的使用
"""

import tensorflow as tf

print 'TensorFlow中变量的自增'

# 创建一个变量
state = tf.Variable(0,name="counter")
# 创建一个op，使得state+1
new_value= tf.add(state,1)
# TensorFLow中的复制操作,将new_value to state
update = tf.assign(state,new_value)

init = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(init)
    for _ in range(5):
        session.run(update)
        print 'state = ' , session.run(state)

