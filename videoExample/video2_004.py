#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: video2_001.py
@time: 2018/2/25 09:54
TensorFlow中变量的使用
"""

import tensorflow as tf

print 'TensorFlow Fetch and Feed'

# define 3 constance
input1  = tf.constant(3.0)
input2  = tf.constant(4.0)
input3  = tf.constant(5.0)

add = tf.add(input2,input3)
mul = tf.multiply(input1,add)


# define tf.placeholder()
input4 = tf.placeholder(tf.float32)
input5 = tf.placeholder(tf.float32)
add_pla = tf.add(input4,input5)
feed = {input4:2.4,input5:8.4}


with tf.Session() as session:
    print  'Fetch:this result = ' , session.run([mul,add])
    print '---------------------------'
    print  'Feed:this result = ',session.run(add_pla,feed_dict=feed)
