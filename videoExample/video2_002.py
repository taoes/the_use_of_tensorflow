#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: video2_001.py
@time: 2018/2/25 09:54
TensorFlow中变量的使用
"""

import tensorflow as tf

print 'TensorFlow中变量的使用法'

x = tf.Variable([1,2])
a = tf.constant([3,3])

sub = tf.subtract(x,a)
add= tf.add(x,a)

init = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(init)
    print '减法计算结果是:',session.run(sub)
    print '加法计算结果是:',session.run(add)

