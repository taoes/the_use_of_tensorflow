#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: video2_001.py
@time: 2018/2/25 09:54
"""

import tensorflow as tf

m1 = tf.constant([[3,3]])
m2 = tf.constant([[2],[3]])
m3 = tf.multiply(m1,m2)
print '这是错误的输出范例:',m3
with tf.Session() as session:
    print '这是正确的输出范例:',session.run(m3)