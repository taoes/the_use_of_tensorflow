#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: index.py
@time: 2018/2/24 14:47
"""

# 学习内容:变量的使用方法

import tensorflow as tf

if __name__ == '__main__':
    x1 = tf.Variable(2.0, dtype=tf.float32, name='x_number_1')
    x2 = tf.Variable([34.0,43.65], dtype=tf.float32, name='x_number_2')
    x3 = tf.Variable([[34.0, 23.1], [1.2, 12.3]], dtype=tf.float32, name='x_number_3')
    x4 = tf.Variable(tf.zeros([4, 3]), dtype=tf.float32, name='x_number_4')
    mul = tf.multiply(x2,x3)
    normal = tf.Variable(10,name='normalVariable')
    normal_new = normal.assign(100)
    # 准备初始化全局变量
    init = tf.global_variables_initializer()
    with tf.Session() as session:
        # 初始化全部的变量信息
        session.run(init)
        print '矩阵计算结果是:',session.run(mul)
        print '生成4*3的矩阵:',session.run(x4)
        print '执行操作之前变量normal的值为:',normal.eval()
        print '从Session中执行的返回值是',session.run(normal_new)
        print '执行操作之后变量normal的值为:',normal.eval()

