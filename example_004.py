#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: index.py
@time: 2018/2/24 14:47
"""


# 学习内容:常见的tensorFlow操作以及绘制图的使用方法


import tensorflow as tf
#运行完成之后，终端输入tensorboard --logdir="./graphs"后在浏览中
#访问localhost:6006
if __name__ == '__main__':
    x = tf.constant(3.0,dtype=tf.float32,name='num1')
    y = tf.constant(4.0,dtype=tf.float32,name='num2')
    #5
    add_op = tf.add(x, y,name="add_op")

    #-1
    sub_op = tf.subtract(x,y,name="sub_op")
    #6
    mul_op = tf.multiply(x, y,name="mul_op")
    #10
    useless = tf.multiply(sub_op, add_op,name="useless")
    #5^10
    pow_op = tf.pow(add_op, mul_op,name="pow_op")

    result = tf.multiply(useless,pow_op,name="findlyResult")
    with tf.Session() as sess:
        sess.run(pow_op)
        writer = tf.summary.FileWriter('./graphs', sess.graph)
        print(sess.run(add_op))
    writer.close()

    # close the writer when you’re done using it
