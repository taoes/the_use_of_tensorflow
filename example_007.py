#!/usr/bin/env python
# encoding: utf-8


"""
@version: ??
@author: 周涛
@license: Apache Licence 
@contact: zhoutao@xiaodouwangluo.com
@site: http://www.zhoutaotao.xyz
@software: PyCharm
@file: index.py
@time: 2018/2/24 14:47
"""

# 学习内容:占位符的使用方法
import tensorflow as tf

if __name__ == '__main__':
    num1 = tf.placeholder(dtype=tf.float32,shape=[3],name='unknowdata')
    num2 = tf.Variable(10,dtype=tf.float32,name='mul_sc')
    num3 = tf.multiply(num2,num1)
    init = tf.global_variables_initializer()
    with tf.Session() as session:
        session.run(init)
        result = session.run(num3,feed_dict={num1:[1.1,2.2,3.3]})
        print result

