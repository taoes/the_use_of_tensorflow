# TensorFlow的使用
+ [TensorFlow的基本使用方法](https://gitee.com/zhoutao825638/the_use_of_tensorflow/blob/master/example_001.py)

+ [构建Session的第二种方法](https://gitee.com/zhoutao825638/the_use_of_tensorflow/blob/master/example_002.py)

+ [常见的tensorFlow的数据执行方法](https://gitee.com/zhoutao825638/the_use_of_tensorflow/blob/master/example_003.py)

+ [常见的tensorFlow操作以及绘制图的使用方法](https://gitee.com/zhoutao825638/the_use_of_tensorflow/blob/master/example_004.py)

+ [向量和矩阵的生成方法](https://gitee.com/zhoutao825638/the_use_of_tensorflow/blob/master/example_005.py)

+ [变量的使用方法](https://gitee.com/zhoutao825638/the_use_of_tensorflow/blob/master/example_006.py)

+ [占位符的使用方法](https://gitee.com/zhoutao825638/the_use_of_tensorflow/blob/master/example_007.py)
